package unittesting.hnh.unittesting.Activity.Login.Interactor;

/**
 * Created by HNH on 4/19/2016.
 */
public interface LoginInteractor {
    boolean isValidLogin(String username, String password);
}
