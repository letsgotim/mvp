package unittesting.hnh.unittesting.Activity.Login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import unittesting.hnh.unittesting.Activity.Login.Presenter.LoginPresenterImpl;
import unittesting.hnh.unittesting.Activity.Login.Interactor.LoginInteractorImpl;
import unittesting.hnh.unittesting.Activity.Login.Presenter.LoginView;
import unittesting.hnh.unittesting.R;

public class Login extends AppCompatActivity implements LoginView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txt_username)
    EditText txtUsername;
    @Bind(R.id.txt_password)
    EditText txtPassword;
    @Bind(R.id.btn_login)
    Button btnLogin;

    private LoginPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();

        presenter = new LoginPresenterImpl(this, new LoginInteractorImpl());
    }

    private void init() {
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.btn_login)
    void clickLogin() {
        presenter.requestLogin();
    }

    @Override
    public String getUsername() {
        return txtUsername.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {
        txtUsername.setError(getString(resId));
    }

    @Override
    public String getPassword() {
        return txtPassword.getText().toString();
    }

    @Override
    public void showPasswordError(int resId) {
        txtPassword.setError(getString(resId));
    }

    @Override
    public void showSuccess() {
        Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoginFail() {
        Toast.makeText(this, "Login Fail", Toast.LENGTH_SHORT).show();
    }
}
