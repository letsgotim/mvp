package unittesting.hnh.unittesting.Activity.Login.Presenter;

/**
 * Created by HNH on 4/19/2016.
 */
public interface LoginPresenter {
    void requestLogin();
}
