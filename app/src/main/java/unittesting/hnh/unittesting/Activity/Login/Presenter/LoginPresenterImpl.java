package unittesting.hnh.unittesting.Activity.Login.Presenter;

import unittesting.hnh.unittesting.Activity.Login.Interactor.LoginInteractor;
import unittesting.hnh.unittesting.Activity.Login.Interactor.LoginInteractorImpl;
import unittesting.hnh.unittesting.R;

/**
 * Created by HNH on 4/19/2016.
 */
public class LoginPresenterImpl implements LoginPresenter{
    private LoginView view;
    private LoginInteractorImpl service;

    public LoginPresenterImpl(LoginView view, LoginInteractorImpl service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void requestLogin() {
        String username = view.getUsername();
        String password = view.getPassword();

        if (username.isEmpty()) {
            view.showUsernameError(R.string.username_error);
            return;
        }
        if (password.isEmpty()) {
            view.showPasswordError(R.string.password_error);
            return;
        }

        boolean login = service.isValidLogin(username, password);
        if (login) {
            view.showSuccess();
            return;
        }
        view.showLoginFail();
    }
}
