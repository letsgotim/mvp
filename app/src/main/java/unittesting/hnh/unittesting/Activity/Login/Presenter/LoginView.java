package unittesting.hnh.unittesting.Activity.Login.Presenter;

/**
 * Created by HNH on 4/19/2016.
 */
public interface LoginView {
    String getUsername();

    void showUsernameError(int resId);

    String getPassword();

    void showPasswordError(int resId);

    void showSuccess();

    void showLoginFail();
}
