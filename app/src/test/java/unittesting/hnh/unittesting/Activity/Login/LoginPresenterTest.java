package unittesting.hnh.unittesting.Activity.Login;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import unittesting.hnh.unittesting.Activity.Login.Presenter.LoginPresenterImpl;
import unittesting.hnh.unittesting.Activity.Login.Interactor.LoginInteractorImpl;
import unittesting.hnh.unittesting.Activity.Login.Presenter.LoginView;
import unittesting.hnh.unittesting.R;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by HNH on 4/19/2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    @Mock
    private LoginView view;
    @Mock
    private LoginInteractorImpl service;

    private LoginPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new LoginPresenterImpl(view, service);
    }

    @Test
    public void shouldShowErrorMessageWhenUsernameIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("");
        presenter.requestLogin();

        verify(view).showUsernameError(R.string.username_error);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("tim");
        when(view.getPassword()).thenReturn("");
        presenter.requestLogin();

        verify(view).showPasswordError(R.string.password_error);
    }

    @Test
    public void shouldShowSuccessWhenUsernameAndPasswordIsCorrect() throws Exception {
        when(view.getUsername()).thenReturn("tim");
        when(view.getPassword()).thenReturn("123");
        when(service.isValidLogin("tim", "123")).thenReturn(true);
        presenter.requestLogin();

        verify(view).showSuccess();
    }

    @Test
    public void shouldShowSuccessWhenUsernameAndPasswordIsInvalid() throws Exception {
        when(view.getUsername()).thenReturn("tim");
        when(view.getPassword()).thenReturn("1234");
        when(service.isValidLogin("tim","123")).thenReturn(true);
        presenter.requestLogin();

        verify(view).showLoginFail();
    }
}